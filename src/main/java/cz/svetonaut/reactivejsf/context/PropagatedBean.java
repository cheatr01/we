package cz.svetonaut.reactivejsf.context;

/**
 *
 * @author sn
 */
public interface PropagatedBean {

    String getUuid();

    void notifyObserver(SessionBoundDto dto);

}
