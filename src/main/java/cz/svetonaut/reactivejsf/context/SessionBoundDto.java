package cz.svetonaut.reactivejsf.context;

import cz.svetonaut.reactivejsf.Message;

/**
 *
 * @author sn
 */
public interface SessionBoundDto {

    String getSessionUuid();

    Message getMessage();

}
