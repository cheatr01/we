package cz.svetonaut.reactivejsf.context;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

/**
 *
 * @author sn
 */
@ApplicationScoped
public class PropagationContext {

    private static final Logger LOGGER = Logger.getLogger(PropagationContext.class.getName());

    private final Map<String, PropagatedBean> collection = new HashMap<>();

    public boolean register(PropagatedBean bean) {
        String uuid = bean.getUuid();
        PropagatedBean puted = collection.put(uuid, bean);
        return puted == null;
    }

    public boolean unregister(PropagatedBean bean) {
        String uuid = bean.getUuid();
        PropagatedBean removed = collection.remove(uuid);
        return removed == null;
    }

    private void search(String uuid) {

    }

    public void observe(@Observes @Propagation SessionBoundDto dto) {
        String uuid = dto.getSessionUuid();
        PropagatedBean bean = collection.get(uuid);
        if (bean == null) {
            LOGGER.warning("Bean of uuid " + uuid + " non exist in PropagationContext");
            return;
        }
        bean.notifyObserver(dto);
    }

}
