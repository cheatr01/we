package cz.svetonaut.reactivejsf.context;

import cz.svetonaut.reactivejsf.Message;

/**
 *
 * @author sn
 */
public class SessionMessageDto implements SessionBoundDto {

    private Message message;

    private String uuid;

    public SessionMessageDto(Message message, String uuid) {
        this.message = message;
        this.uuid = uuid;
    }

    @Override
    public String getSessionUuid() {
        return uuid;
    }

    @Override
    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
