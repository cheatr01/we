package cz.svetonaut.reactivejsf;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sn
 */
@Named(value = "messageBean")
@SessionScoped
public class MessageBean implements Serializable {

    @NotNull(message = "Tak se snad nějak jmenuješ ne?!?")
//    @Pattern(regexp = !("[pP]uv[aá]k"), message = "Jo tak Puvák jo? Tak tebe tady tuplem nechceme ksindle!")
    private String name;

    @NotNull(message = "Tady žádný malomluvky nechceme!")
    private String text;

    @NotNull
    private LocalDateTime date;

    private Message msg;

    @Inject
    private ReactiveBean reactiveBean;

    @Inject
    private BeanManager beanManager;

    public void sendMessage() {
        createMessage();
        reactiveBean.addMessage(msg);
        reactiveBean.push();
    }

    public void fireEvent() throws InterruptedException {
        Thread.sleep(1000);
        beanManager.fireEvent("hohoho");
    }

    private void onEvent(@Observes String s) {
        reactiveBean.addMessage(new Message("event", "event text", LocalDateTime.now()));
        reactiveBean.push();
    }

    private void createMessage() {
        date = LocalDateTime.now();
        msg = new Message(name, text, date);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}
