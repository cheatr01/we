package cz.svetonaut.reactivejsf;

import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

/**
 *
 * @author sn
 */
@Named(value = "reactiveBean")
@ApplicationScoped
public class ReactiveBean {

    private List<Message> messages = new ArrayList<>();

    @Inject
    @Push(channel = "chnl")
    private PushContext update;

    public void addMessage(Message msg) {
        if (msg != null) {
            messages.add(msg);
        }
    }


    public void push() {
        update.send("ggg");
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

}
