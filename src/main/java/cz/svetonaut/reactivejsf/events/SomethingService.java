package cz.svetonaut.reactivejsf.events;

import cz.svetonaut.reactivejsf.Message;
import cz.svetonaut.reactivejsf.context.Propagation;
import cz.svetonaut.reactivejsf.context.SessionBoundDto;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

/**
 *
 * @author sn
 */
@Stateless
public class SomethingService {

    @Inject
    @SessOnly
    private Event<Message> event;

    @Inject
    @Propagation
    private Event<SessionBoundDto> propagatedEvent;

    @Inject
    private EventSessionBean sessionBean;

    public void fireEventSynchro(Message msg) {
        fireEvent(msg);
    }

    private void fireEvent(Message msg) {
        event.fire(msg);
    }

    @Asynchronous
    public void fireEventAsync(Message msg) {
        fireEvent(msg);
    }

    @Asynchronous
    public void firePropagatedEventAsync(SessionBoundDto dto) {
        propagatedEvent.fire(dto);
    }

}
