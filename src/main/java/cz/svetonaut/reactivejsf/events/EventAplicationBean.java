package cz.svetonaut.reactivejsf.events;

import cz.svetonaut.reactivejsf.Message;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

/**
 *
 * @author sn
 */
@Named(value = "eventAplicationBean")
@ApplicationScoped
public class EventAplicationBean {

    @Inject
    @Push
    private PushContext appEvent;

    @Inject
    private Event<Message> event;

    private List<Message> appEvents = new ArrayList<>();

    public List<Message> getAppEvents() {
        return appEvents;
    }

    public void setAppEvents(List<Message> appEvents) {
        this.appEvents = appEvents;
    }

    public void addMessage(Message msg) {
        appEvents.add(msg);
    }

    public void handleEvent(@Observes Message msg) {
        System.out.println("invoke application event handler");
        addMessage(msg);
        appEvent.send("appEvent");
    }

    void fireEvent(Message message) {
        event.fire(message);
    }

}
