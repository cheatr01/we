package cz.svetonaut.reactivejsf.events;

import cz.svetonaut.reactivejsf.Message;
import cz.svetonaut.reactivejsf.context.PropagatedBean;
import cz.svetonaut.reactivejsf.context.PropagationContext;
import cz.svetonaut.reactivejsf.context.SessionBoundDto;
import cz.svetonaut.reactivejsf.context.SessionMessageDto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

/**
 *
 * @author sn
 */
@Named(value = "eventSessionBean")
@SessionScoped
public class EventSessionBean implements Serializable, PropagatedBean {

    @Inject
    @Push
    private PushContext sessionEvent;

    @Inject
    @SessOnly
    private Event<Message> event;

    private List<Message> sessionEvents = new ArrayList<>();

    @EJB
    private SomethingService somethingService;

    @Inject
    private PropagationContext propagationContext;

    private String uuid;

    @PostConstruct
    private void init() {
        uuid = UUID.randomUUID().toString();
        propagationContext.register(this);
    }

    public List<Message> getSessionEvents() {
        return sessionEvents;
    }

    public void setSessionEvents(List<Message> sessionEvents) {
        this.sessionEvents = sessionEvents;
    }

    public void addMessage(Message msg) {
        sessionEvents.add(msg);
    }

//    public void handleEvent(@Observes Message msg) {
//        handle(msg);
//    }

    public void handleSessEvent(@Observes @SessOnly Message msg) {
        handle(msg);
    }

    public void handle(Message msg) {
        System.out.println("invoke session event handler");
        addMessage(msg);
        sessionEvent.send("sessionEvent");
    }

    void fireEvent(Message message) {
        event.fire(message);
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public void notifyObserver(SessionBoundDto dto) {
        Message message = dto.getMessage();
        addMessage(message);
        sessionEvent.send("sessionEvent");
    }

    public void firePropagatingAsyncEjbEvent() {
        somethingService.firePropagatedEventAsync(new SessionMessageDto(new Message("Propagated event", "propagated", LocalDateTime.now()), uuid));
    }

}
