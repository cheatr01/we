package cz.svetonaut.reactivejsf.events;

import cz.svetonaut.reactivejsf.Message;
import cz.svetonaut.reactivejsf.ReactiveBean;
import cz.svetonaut.reactivejsf.context.PropagatedBean;
import cz.svetonaut.reactivejsf.context.PropagationContext;
import cz.svetonaut.reactivejsf.context.SessionBoundDto;
import cz.svetonaut.reactivejsf.context.SessionMessageDto;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

/**
 *
 * @author sn
 */
@Named(value = "eventsBean")
@RequestScoped
public class EventsBean {

    @Inject
    private BeanManager beanManager;

    @Inject
    private EventAplicationBean aplicationBean;

    @Inject
    private EventSessionBean sessionBean;

    @Inject
    private EventSessionBean eventSessionBean;

    @EJB
    private SomethingService somethingService;

    public void fireRequestEvent() {
        beanManager.fireEvent(new Message("request event", "request event", LocalDateTime.now()));
    }

    public void fireSessionEvent() {
        sessionBean.fireEvent(new Message("session event", "session event", LocalDateTime.now()));
    }

    public void fireApplicationEvent() {
        aplicationBean.fireEvent(new Message("application event", "application event", LocalDateTime.now()));
    }

    public void fireEjbEvent() {
        somethingService.fireEventSynchro(new Message("EJB event", "application event", LocalDateTime.now()));
    }

    public void fireAsyncEjbEvent() {
        somethingService.fireEventAsync(new Message("Async EJB event", "application event", LocalDateTime.now()));
    }

    public void firePropagatingAsyncEjbEvent() {
        eventSessionBean.firePropagatingAsyncEjbEvent();
    }

}
