package cz.svetonaut.reactivejsf;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

/**
 *
 * 
 */
@Named
@SessionScoped
public class SessionBean implements Serializable{

    private List<Message> sessMessages = new ArrayList<>();
    
    @Inject @Push(channel = "session")
    private PushContext sess;
    
    public void sessPush() {
        sessMessages.add(new Message("sess", "sess text", LocalDateTime.now()));
        sess.send("eventName");
    }

    public List<Message> getSessMessages() {
        return sessMessages;
    }

    public void setSessMessages(List<Message> sessMessages) {
        this.sessMessages = sessMessages;
    }
    
    
    
}
