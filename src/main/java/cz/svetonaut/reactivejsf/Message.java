package cz.svetonaut.reactivejsf;

import java.time.LocalDateTime;
import javax.inject.Named;

/**
 *
 * @author sn
 */
public class Message {

    private final String name;

    private final String text;

    private final LocalDateTime dateOfSent;

    /**
     *
     * @param name
     * @param text
     * @param dateOfSent
     */
    public Message(String name, String text, LocalDateTime dateOfSent) {
        this.name = name;
        this.text = text;
        this.dateOfSent = dateOfSent;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public LocalDateTime getDateOfSent() {
        return dateOfSent;
    }

}
